# Overview

2SReLU paper source code.


# Dependencies
 - Python 3
   - numpy          1.18.1
   - torch           1.4.0
   - torchvision     0.5.0
   - PyYAML          5.3.1
 - cuda (optional)    10.*

## Installing with pip

```bash
  $ pip install -r requirements.txt
```


# Getting started
 - Install the dependencies
 - Train/test the 2SReLU mnist example:

```bash
  $ python3 scripts/train.py
```

 - To train/test the spatial network baseline:

```bash
  $ python3 scripts/train.py --network=cnn
```


# Parameters
 - The training/testing paremeters are in: `scripts/params.yml`
 - To train with cuda support, set the device parameter to "cuda"
   - It requires a large amount of memory (~4GB) and may be slower
