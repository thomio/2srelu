import numpy as np
import random
import time
import yaml
import argparse

import torch
import torchvision.datasets as datasets

from networks import lenet
from datasets import mnist, mnist_fft



def print_confusion_matrix(confusion_matrix):
    print("Per-class confusion matrix:")
    header = ["class", "TP", "TN", "FP", "FN", "Precision", "Recall", "F1"]
    print("".join([i.rjust(10) for i in header]))
    for i in range(10):
        TP = confusion_matrix[i]["TP"]
        TN = confusion_matrix[i]["TN"]
        FP = confusion_matrix[i]["FP"]
        FN = confusion_matrix[i]["FN"]
        Precision = TP/(TP+FP)
        Recall = TP/(TP+FN)
        F1 = 2 * (Precision * Recall) / (Precision + Recall)
        table_row = [str(i), str(TP), str(TN), str(FP), str(FN),
                    "{:.4f}".format(Precision),
                    "{:.4f}".format(Recall),
                    "{:.4f}".format(F1)]
        print("".join([i.rjust(10) for i in table_row]))
    return


def compute_confusion_matrix(confusion_matrix, prediction, condition):
    for i in range(10): # goes from class 0 to 9
        # return True whem condition == i
        class_i_index = (condition == i)
        # count number of elements
        num_positive_instances = class_i_index.sum().item()
        if num_positive_instances > 0:
            TP = (prediction[class_i_index] == i).sum().item()
            FN = num_positive_instances - TP
            confusion_matrix[i]["TP"] += TP
            confusion_matrix[i]["FN"] += FN
        not_class_i_index = (condition != i)
        num_negative_instances = not_class_i_index.sum().item()
        if num_negative_instances > 0:
            FP = (prediction[num_negative_instances] == i).sum().item()
            TN = num_negative_instances - FP
            confusion_matrix[i]["FP"] += FP
            confusion_matrix[i]["TN"] += TN
    return confusion_matrix


def test_net(net, test_dataset_fft, test_batch_size=1000):
    confusion_matrix = [{"TP": 0, "FN": 0, "FP": 0, "TN": 0} for i in range(10)]
    confidence = 0.0
    with torch.no_grad():
        correct = 0
        test_size = len(test_dataset_fft)
        num_batches = int(test_size/test_batch_size)
        for i in range(num_batches):
            images_batch = list()
            labels_batch = list()
            for j in range(test_batch_size):
                k = i*test_batch_size + j
                test_image, test_label = test_dataset_fft[k]
                images_batch.append(test_image)
                labels_batch.append(test_label)
            outputs = net(torch.stack(images_batch))
            predicted = torch.argmax(outputs.data, 1)
            correct += (predicted == torch.stack(labels_batch)).sum().item()
            confusion_matrix = \
              compute_confusion_matrix(confusion_matrix, predicted, torch.stack(labels_batch))
        confidence = (100 * correct)/test_size
    return confidence, confusion_matrix


def read_params(network, file_name = "scripts/params.yml"):
    print("Reading parameters file: {}".format(file_name))
    try:
        file_handle = open(file_name)
        params = yaml.load(file_handle, Loader=yaml.FullLoader)[network]
        for key in params:
            print('\t{} = {}'.format(key, params[key]))
    except IOError:
        print("Error: could not open parameters file")
        exit()
    return params


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='2SReLU source code')
    parser.add_argument('--network', type=str, default="snn", metavar="cnn",
                        help='network type (default: snn)')
    args = parser.parse_args()
    network_type = args.network
    print("Network: {}".format(network_type))

    params = read_params( network_type )
    device = torch.device(params['device'])
    batch_size = params['batch_size']
    lr = params['learning_rate']
    num_iterations = params['num_iterations']
    print_interval = params['print_interval']
    test_interval = params['test_interval']
    save_data = params['save_data']
    randomize = params['randomize_data']

    train_dataset = datasets.MNIST(root='./dataset', train=True,
                                   download=True, transform=None)
    test_dataset = datasets.MNIST(root='./dataset', train=False,
                                  download=True, transform=None)

    train_size = len(train_dataset)
    if network_type == "snn":
        train_dataset_fft = mnist_fft.transform_dataset(device, train_dataset)
        test_dataset_fft = mnist_fft.transform_dataset(device, test_dataset)
        net = lenet.SNN().to(device)
    elif network_type == "cnn":
        train_dataset_fft = mnist.transform_dataset(device, train_dataset)
        test_dataset_fft = mnist.transform_dataset(device, test_dataset)
        net = lenet.CNN().to(device)

    if save_data:
        acc_file = open("data.txt", "w")
        acc_file.write("# iteration accuracy\n")

    running_loss = 0.0
    accumulated_time = 0.0
    highest_acc = 0.0

    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(net.parameters(), lr, momentum=0.9)
    print('\nTraining network...')
    for i in range(num_iterations):
        start_time = time.time()
        images_batch = list()
        labels_batch = list()
        for j in range(batch_size):
            epoch, k = divmod(i*batch_size + j, train_size)
            image, label = train_dataset_fft[k]
            images_batch.append(image)
            labels_batch.append(label)
        outputs = net(torch.stack(images_batch))
        loss = criterion(outputs, torch.stack(labels_batch))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        running_loss += loss.item()
        iteration_time = time.time() - start_time
        accumulated_time += iteration_time
        if (i+1) % print_interval == 0:
            msg  = "epoch: {}, ".format(epoch)
            msg += "iteration: {}/{}, ".format(i+1, num_iterations)
            msg += "iteration_time: {:.3f}s, ".format(iteration_time)
            msg += "loss: {:.4f}".format(running_loss/print_interval)
            print(msg)
            running_loss = 0.0
        if (i+1) % test_interval == 0:
            confidence, confusion_matrix = test_net(net, test_dataset_fft)
            if save_data:
                acc_file.write( "{0} {1}\n".format(i+1, confidence) )
                acc_file.flush()
            if confidence > highest_acc:
                highest_acc = confidence
            print("Average iteration time: %.3fs"%(accumulated_time/(i+1)))
            print("Accuracy on test images: %.2f%%"%(confidence))
            print("Highest accuracy: %.2f%%"%(highest_acc))
            print_confusion_matrix(confusion_matrix)
            if randomize:
                # After each test_interval, shuffle the traning data
                random.shuffle(train_dataset_fft)
    print("Done !!!")

