import math
import torch
import torch.nn as nn




class NoBiasSparseFunction(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x, weights):
      output_chs, input_chs, num_rows, num_cols = weights.size()
      batch_size, input_chs, num_rows, num_cols = x.size()
      ctx.save_for_backward(x, weights)
      if weights.is_cuda:
          y = torch.zeros((batch_size, output_chs, num_rows, num_cols),
                        device=torch.cuda.current_device())
      else:
          y = torch.zeros((batch_size, output_chs, num_rows, num_cols))
      for i in range(batch_size):
          for j in range(output_chs):
              y[i,j] = torch.sum(x[i]*weights[j], dim=0)
      return y

    @staticmethod
    def backward(ctx, grad_output):
      x, weights = ctx.saved_tensors
      batch_size, output_chs, num_rows, num_cols = grad_output.size()
      batch_size, input_chs, num_rows, num_cols = x.size()
      if weights.is_cuda:
          grad_input = torch.zeros(x.size(), device=torch.cuda.current_device())
          grad_weights = torch.zeros(weights.size(), device=torch.cuda.current_device())
      else:
          grad_input = torch.zeros(x.size())
          grad_weights = torch.zeros(weights.size())
      for i in range(batch_size):
          for j in range(output_chs):
              grad_input[i] += grad_output[i,j] * weights[j]
              grad_weights[j] += grad_output[i,j] * x[i]
      return grad_input, grad_weights/batch_size


class SparseFunction(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x, weights, bias):
        output_chs, input_chs, num_rows, num_cols = weights.size()
        batch_size, input_chs, num_rows, num_cols = x.size()
        ctx.save_for_backward(x, weights, bias)
        y = torch.zeros((batch_size, output_chs, num_rows, num_cols),
                        device=torch.cuda.current_device())
        for i in range(batch_size):
            for j in range(output_chs):
                y[i,j] = torch.sum(x[i]*weights[j], dim=0) + bias[j]
        return y

    @staticmethod
    def backward(ctx, grad_output):
        x, weights, bias = ctx.saved_tensors
        batch_size, output_chs, num_rows, num_cols = grad_output.size()
        grad_input = torch.zeros(x.size(), device=torch.cuda.current_device())
        grad_weights = torch.zeros(weights.size(), device=torch.cuda.current_device())
        for i in range(batch_size):
            for j in range(output_chs):
                grad_input[i] += grad_output[i,j] * weights[j]
                grad_weights[j] += grad_output[i,j] * x[i]
        grad_bias =  torch.sum(grad_output, dim=0)
        return grad_input, grad_weights/batch_size, grad_bias/batch_size


class Sparse(nn.Module):
    def __init__(self, input_chs, output_chs, num_rows, num_cols, select_bias=False):
        super(Sparse, self).__init__()
        self.select_bias = select_bias
        self.weights = nn.Parameter(torch.Tensor(output_chs, input_chs, num_rows, num_cols))
        if self.select_bias:
            self.bias = nn.Parameter(torch.Tensor(output_chs, num_rows, num_cols))
        else:
            self.register_parameter('bias', None)
        self.init_parameters_uniform()

    def forward(self, x):
        if self.select_bias:
          return SparseFunction.apply(x, self.weights, self.bias)
        else:
          return NoBiasSparseFunction.apply(x, self.weights)

    def init_parameters_kaiming(self):
        nn.init.kaiming_uniform_(self.weights, a=math.sqrt(5))
        if self.bias is not None:
            fan_in, _ = nn.init._calculate_fan_in_and_fan_out(self.weights)
            bound = 1 / math.sqrt(fan_in)
            nn.init.uniform_(self.bias, -bound, bound)

    def init_parameters_uniform(self):
        self.weights.data.uniform_(-0.1, 0.1)
        if self.bias is not None:
            self.bias.data.uniform_(-0.1, 0.1)


