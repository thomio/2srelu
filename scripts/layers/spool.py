import torch


class SPool(torch.autograd.Function):
  @staticmethod
  def forward(ctx, x):
    ctx.save_for_backward(x)
    batch_size, num_chs, num_rows, num_cols = x.size()
    cx = int(num_rows/2)
    cy = int(num_cols/2)
    dx = int(num_rows/4)
    dy = int(num_cols/4)
    rem_x, rem_y = 0, 0
    if cx%2 != 0:
        rem_x = 1
    if cy%2 != 0:
        rem_y = 1
    y = x[:,:,cx-dx:cx+dx+rem_x,cy-dy:cy+dy+rem_y]
    return y

  @staticmethod
  def backward(ctx, grad_output):
    x, = ctx.saved_tensors
    _, _, pad_x, pad_y = torch.tensor(x.size()) - torch.tensor(grad_output.size())
    pad_col = int(pad_y/2)
    pad_row = int(pad_x/2)
    rem_col, rem_row = 0, 0
    if pad_y%2 != 0:
        rem_col = 1
    if pad_x%2 != 0:
        rem_row = 1
    shape = (pad_col+rem_col, pad_col, pad_row+rem_row, pad_row)
    pad = torch.nn.ZeroPad2d(shape)
    grad_input = pad(grad_output)
    return grad_input
