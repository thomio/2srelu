import torch
import torch.nn as nn


class SRelu(torch.autograd.Function):
  @staticmethod
  def forward(ctx, x):
    batch_size, num_chs, num_rows, num_cols = x.size()
    cx=int(num_rows/2)
    cy=int(num_cols/2)
    idx=torch.zeros((num_rows, num_cols), dtype=torch.bool)
    for i in range(num_rows):
        for j in range(num_cols):
            if (i+cx)%2==0 and (j+cy)%2==0:
                idx[i,j] = 1
    ctx.save_for_backward(x,idx)
    size_x = cx
    size_y = cy
    y = x[:,:,idx].reshape(batch_size, num_chs, size_x, size_y)
    pad_x = num_rows - size_x
    pad_y = num_cols - size_y
    pad_col = int(pad_y/2)
    pad_row = int(pad_x/2)
    rem_col, rem_row = 0, 0
    if pad_y%2 != 0:
        rem_col = 1
    if pad_x%2 != 0:
        rem_row = 1
    shape = (pad_col+rem_col, pad_col, pad_row+rem_row, pad_row)
    pad = nn.ZeroPad2d(shape)
    y = pad(y)
    return .7*x + .3*y

  @staticmethod
  def backward(ctx, grad_output):
    x, idx = ctx.saved_tensors
    if x.is_cuda:
        y = torch.zeros(x.size(), device=torch.cuda.current_device())
    else:
        y = torch.zeros(x.size())
    batch_size, num_chs, num_rows, num_cols = x.size()
    cx = int(num_rows/2)
    cy = int(num_cols/2)
    dx = int(num_rows/4)
    dy = int(num_cols/4)
    rem_x, rem_y = 0, 0
    if cx%2 != 0:
        rem_x = 1
    if cy%2 != 0:
        rem_y = 1
    grads = grad_output[:,:,cx-dx:cx+dx+rem_x,cy-dy:cy+dy+rem_y]
    y[:,:,idx] = torch.flatten(grads, start_dim=2)
    return .7*grad_output + .3*y
