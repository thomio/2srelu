#!/usr/bin/env python
import torch.nn as nn

from layers.spool import *
from layers.srelu import *
from layers.sparse import *



class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        self.relu = nn.ReLU()
        self.pool = nn.MaxPool2d(kernel_size=[2,2], stride=2)
        self.conv1_1 = nn.Conv2d(1, 16, [3,3], padding=1, bias=False)
        self.conv1_2 = nn.Conv2d(16, 16, [3,3], padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(16, affine=False)
        self.conv2_1 = nn.Conv2d(16, 32, [3,3], padding=1, bias=False)
        self.conv2_2 = nn.Conv2d(32, 32, [3,3], padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(32, affine=False)
        self.fc1 = nn.Linear(32*7*7, 512, bias=False)
        self.fc2 = nn.Linear(512, 128, bias=False)
        self.fc3 = nn.Linear(128, 10, bias=False)

    def forward(self, x):
        x = self.relu(self.bn1(self.conv1_1(x)))
        x = self.relu(self.bn1(self.conv1_2(x)))
        x = self.pool(x)
        x = self.relu(self.bn2(self.conv2_1(x)))
        x = self.relu(self.bn2(self.conv2_2(x)))
        x = self.pool(x)
        x = x.contiguous().view(-1, 32*7*7)
        x = self.fc1(x)
        x = self.fc2(x)
        x = self.fc3(x)
        return x


class SNN(nn.Module):
    def __init__(self):
        super(SNN, self).__init__()
        self.pool = nn.MaxPool2d(kernel_size=[2,2], stride=2)
        self.spool = SPool.apply
        self.srelu = SRelu.apply

        self.sparse1_0 = Sparse(2, 2, 32, 32)
        self.bn1_0 = nn.BatchNorm2d(2, affine=False)
        self.sparse1_1 = Sparse(2, 2, 32, 32)
        self.bn1_1 = nn.BatchNorm2d(2, affine=False)
        self.sparse1_2 = Sparse(2, 2, 32, 32)
        self.bn1_2 = nn.BatchNorm2d(2, affine=False)
        self.sparse1_3 = Sparse(2, 2, 32, 32)
        self.bn1_3 = nn.BatchNorm2d(2, affine=False)
        self.sparse1_4 = Sparse(2, 2, 32, 32)
        self.bn1_4 = nn.BatchNorm2d(2, affine=False)

        self.sparse2_1 = Sparse(10, 4, 16, 16)
        self.bn2_1 = nn.BatchNorm2d(4, affine=False)
        self.fc1_size = 4*8*8
        self.fc1 = nn.Linear(self.fc1_size, 10, bias=False)

    def forward(self, x):
        x0 = x[:,0]
        x1 = x[:,1]
        x2 = x[:,2]
        x3 = x[:,3]
        x4 = x[:,4]
        x0 = self.sparse1_0(x0)
        x0 = self.bn1_0(x0)
        x0 = self.srelu(x0)
        x0 = self.spool(x0)
        x1 = self.sparse1_1(x1)
        x1 = self.bn1_1(x1)
        x1 = self.srelu(x1)
        x1 = self.spool(x1)
        x2 = self.sparse1_2(x2)
        x2 = self.bn1_2(x2)
        x2 = self.srelu(x2)
        x2 = self.spool(x2)
        x3 = self.sparse1_3(x3)
        x3 = self.bn1_3(x3)
        x3 = self.srelu(x3)
        x3 = self.spool(x3)
        x4 = self.sparse1_4(x4)
        x4 = self.bn1_4(x4)
        x4 = self.srelu(x4)
        x4 = self.spool(x4)
        x = torch.cat((x0,x1,x2,x3,x4), dim=1)
        x = self.sparse2_1(x)
        x = self.bn2_1(x)
        x = self.srelu(x)
        x = self.spool(x)
        x = x.contiguous().view(-1, self.fc1_size)
        x = self.fc1(x)
        return x
