import numpy as np
import torch
import sys


def transform_dataset(device, original_dataset):
    transformed_dataset = list()
    num_images = len(original_dataset)
    for i, pair in enumerate(original_dataset):
        msg = "\rApplying transformations (please wait): {0}/{1}".format(
            i+1, num_images)
        sys.stdout.write( msg )
        sys.stdout.flush()

        pil_image, label = pair
        image = np.array(pil_image)/np.max(pil_image)
        image = np.expand_dims(image, axis=0)

        new_pair = [torch.from_numpy(image).to(device).float(),
                    torch.tensor(label).to(device).long()]
        transformed_dataset.append(new_pair)
    print('')
    return transformed_dataset