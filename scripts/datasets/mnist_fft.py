import numpy as np
import torch
import sys



def compute_fft(padded_image):
    fft = np.fft.fft2(padded_image)
    fft[0,0] = .0
    fft = np.fft.fftshift(fft)
    fft_abs = np.absolute(fft)
    fft_angle = np.angle(fft)
    # 1e-6 prevents division by zero
    fft_polar = np.stack((fft_abs/np.std(fft_abs+1e-6),
                          fft_angle/np.std(fft_angle+1e-6)))
    return fft_polar


def transform_dataset(device, original_dataset):
    dataset_fft = list()
    num_images = len(original_dataset)
    for i, pair in enumerate(original_dataset):
        msg = "\rApplying transformations (please wait): {0}/{1}".format(
            i+1, num_images)
        sys.stdout.write( msg )
        sys.stdout.flush()
        pil_image, label = pair
        image = np.array(pil_image)

        padded_image = np.pad(image, (2,2), 'constant')
        fft_polar_0 = compute_fft(padded_image)

        padded_image = np.pad(image[8:16,8:16], (12,12), 'constant')
        fft_polar_1 = compute_fft(padded_image)

        padded_image = np.pad(image[8:16,13:21], (12,12), 'constant')
        fft_polar_2 = compute_fft(padded_image)

        padded_image = np.pad(image[13:21,8:16], (12,12), 'constant')
        fft_polar_3 = compute_fft(padded_image)

        padded_image = np.pad(image[13:21,13:21], (12,12), 'constant')
        fft_polar_4 = compute_fft(padded_image)

        polar_input = np.stack((fft_polar_0, fft_polar_1, fft_polar_2, fft_polar_3, fft_polar_4))
        pair_fft = [torch.from_numpy(polar_input).to(device).float(),
                    torch.tensor(label).to(device).long()]
        dataset_fft.append(pair_fft)
    print('')
    return dataset_fft